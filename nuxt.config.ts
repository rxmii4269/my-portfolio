// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  future: {
    typescriptBundlerResolution: true,
  },
  vite: {
    build: {
      rollupOptions: {
        external: ['sharp',],
      },
    },
  },
  nitro: {
    devProxy: {
      host: '127.0.0.1',
    },
  },
  router: {
    options: {
      scrollBehaviorType: 'smooth',
    },
  },
  app: {
    head: {
      script: [
        {
          type: 'text/partytown',
          children: `(function(c,l,a,r,i,t,y){        c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};        t=l.createElement(r);t.async=1;t.src="https://www.clarity.ms/tag/"+i+"?ref=bwt";        y=l.getElementsByTagName(r)[0];y.parentNode.insertBefore(t,y);    })(window, document, "clarity", "script", "ltusf0e4nb");`,
        },
      ],
      meta: [
        {
          name: 'google-site-verification',
          content: 'YYaDovPK2lbt4elQya-alXX2NQh_lyVXsKTeV9ouofM',
        },
      ],
      link: [
        {
          rel: 'icon',
          type: 'image/x-icon',
          href: '/favicon.ico',
        },
        {
          rel: 'icon',
          type: 'image/svg+xml',
          href: '/favicon.svg',
        },
        {
          rel: 'alternate icon',
          type: 'image/png',
          href: '/favicon.png',
        },
      ],
      bodyAttrs: {
        class:
          'bg-slate-900 leading-relaxed text-slate-400 antialiased selection:bg-teal-300 selection:text-teal-900',
      },
      htmlAttrs: {
        'data-theme': 'night',
        class: 'scroll-smooth',
        lang: 'en',
      },
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
    },
  },
  devtools: { enabled: true, },
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/fontaine',
    '@nuxt/fonts',
    '@nuxt/image',
    '@zadigetvoltaire/nuxt-gtm',
    '@nuxtjs/seo',
    '@nuxt/eslint',
    '@nuxtjs/partytown',
  ],
  fontMetrics: {
    fonts: ['Inter',],
  },
  css: ['~/assets/css/main.css',],
  partytown: {
    debug: process.env.NODE_ENV === 'development',
  },
  gtm: {
    scriptType: 'text/partytown',
    id: 'GTM-KPTZ2ZK',
    compatibility: true,
    devtools: true,
    debug: process.env.NODE_ENV === 'development',
    enabled: true,
  },
  site: {
    url: 'https://romainemurray.vercel.app',
    name: 'Romaine Murray | My Portfolio',
    description:
      'My name is Romaine Murray and I am a full stack developer. I am passionate about creating beautiful and functional websites and applications. I am proficient in HTML, CSS, JavaScript, React, Vue, Node.js, Express, MongoDB, and more. I am currently seeking a full-time position as a developer.',
    defaultLocale: 'en',
    debug: process.env.NODE_ENV === 'development',
  },
  seo: {
    redirectToCanonicalSiteUrl: true,
    debug: process.env.NODE_ENV === 'development',
    splash: true,
  },
  eslint: {
    config: {
      stylistic: {
        indent: 2,
        semi: true,
        commaDangle: 'always',
        quotes: 'single',
        quoteProps: 'as-needed',
      },
    },
  },
},);
