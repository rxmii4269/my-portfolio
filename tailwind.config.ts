import type { Config } from 'tailwindcss'
import colors from 'tailwindcss/colors'
import { iconsPlugin, dynamicIconsPlugin } from '@egoist/tailwindcss-icons'
import { fontFamily } from 'tailwindcss/defaultTheme'
export default <Partial<Config>>{
    daisyui: {
        themes: ['night'],
    },
    plugins: [
        require('daisyui'),
        iconsPlugin(),
        dynamicIconsPlugin(),
    ],
    content: [
        'components/**/*.{vue,js,ts}',
        'layouts/**/*.vue',
        'pages/**/*.vue',
        'composables/**/*.{js,ts}',
        'plugins/**/*.{js,ts}',
        'App.{js,ts,vue}',
        'app.{js,ts,vue}',
        'Error.{js,ts,vue}',
        'error.{js,ts,vue}',
        'content/**/*.md',
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Inter', 'Inter fallback', ...fontFamily.sans],
            },
            colors: {
                primary: colors.blue,
            },
        },
    },
}
