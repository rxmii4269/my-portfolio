import withNuxt from './.nuxt/eslint.config.mjs';

export default withNuxt([
  {
    rules: {
      'no-console': ['error', { allow: ['warn', 'error',], },],
      curly: 'error',
      eqeqeq: 'error',
      'no-eq-null': 'error',
      'no-use-before-define': ['error', 'nofunc',],
      'one-var': ['error', 'never',],
      'no-duplicate-imports': 'error',
      'no-useless-constructor': 'error',
      'no-var': 'error',
      'prefer-const': 'error',
      'prefer-template': 'error',
      'vue/no-v-html': 'off',
    },
  },
],);
